// 1.

function result(num) {
    let numDiv3 = num % 3 == 0;
    let numDiv5 = num % 5 == 0;
    if (numDiv3 && numDiv5) {
        return "FizzBuzz";
    } else if (numDiv3) {
        return "Fizz";
    } else if (numDiv5) {
        return "Buzz";
    } else {
        return "Pop";
    }
}

console.log(result(5));
console.log(result(30));
console.log(result(27));
console.log(result(17));



// 2.

function color(letter) {
    if (letter.toLowerCase() == "r") {
        return "Red";
    } else if (letter.toLowerCase() == "o") {
        return "Orange";
    } else if (letter.toLowerCase() == "y") {
        return "Yellow";
    }  else if (letter.toLowerCase() == "g") {
        return "Green";
    } else if (letter.toLowerCase() == "b") {
        return "Blue";
    } else if (letter.toLowerCase() == "i") {
        return "Indigo";
    }  else if (letter.toLowerCase() == "v") {
        return "Violet";
    } else {
        return "No color"
    }
}

console.log(color("r"));
console.log(color("G"));
console.log(color("x"));
console.log(color("B"));
console.log(color("i"));



// STRETCH GOAL

function leapYear(year) {
    if (year % 4 !== 0) {
        return "Not a Leap year";
    } else if (year % 100 == 0 && year % 400 !== 0){
        return "Not a Leap year";
    } else {
        return "Leap Year"
    }
}

console.log(leapYear(1900));
console.log(leapYear(2000));
console.log(leapYear(2004));
console.log(leapYear(2021));